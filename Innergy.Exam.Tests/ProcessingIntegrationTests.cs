using System.Collections.Generic;
using Xunit;

namespace Innergy.Exam.Tests
{
    public class ProcessingIntegrationTests
    {
        private IEnumerable<string> TestRecords() =>
            new string[]
            {
                "# Material inventory initial state as of Jan 01 2018",
                "# New materials",
                "Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10",
                "Maple Dovetail Drawerbox;COM-124047;WH-A,15",
                "Generic Wire Pull;COM-123906c;WH-A,10|WH-B,6|WH-C,2",
                "Yankee Hardware 110 Deg. Hinge;COM-123908;WH-A,10|WH-B,11",
                "# Existing materials, restocked",
                "Hdw Accuride CB0115-CASSRC - Locking Handle Kit - Black;CB0115-CASSRC;WH-C,13|WH-B,5",
                "Veneer - Charter Industries - 3M Adhesive Backed - Cherry 10mm - Paper Back;3M-Cherry-10mm;WH-A,10|WH-B,1",
                "Veneer - Cherry Rotary 1 FSC;COM-123823;WH-C,10",
                "MDF, CARB2, 1 1/8';COM-101734;WH-C,8"
            };

        [Fact]
        void ProcessMaterialsRecords_AndReturnsWarehousesSummary()
        {
            var inputProcessor = new InputProcessor();
            var outputProcessor = new OutputProcessor();

            var materials = inputProcessor.ProcessRecordsToWarehouseMaterials(TestRecords);
            var result = outputProcessor.GetWarehousesSummary(materials);

            var expected = "\nWH-A (total 50)\n3M-Cherry-10mm: 10\nCOM-100001: 5\nCOM-123906c: 10\nCOM-123908: 10\nCOM-124047: 15\n\nWH-C (total 33)\nCB0115-CASSRC: 13\nCOM-101734: 8\nCOM-123823: 10\nCOM-123906c: 2\n\nWH-B (total 33)\n3M-Cherry-10mm: 1\nCB0115-CASSRC: 5\nCOM-100001: 10\nCOM-123906c: 6\nCOM-123908: 11\n";

            Assert.Equal(expected, result);
        }
    }
}