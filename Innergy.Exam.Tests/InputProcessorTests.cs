using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Innergy.Exam.Tests
{
    public class InputProcessorTests
    {
        private IEnumerable<string> TestRecords() =>
            new string[]
            {
                "# Material inventory initial state as of Jan 01 2018",
                "Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10",
                "Maple Dovetail Drawerbox;COM-124047;WH-A,15"
            };

        [Fact]
        void RecordsProcessing_ShouldReturnEmptyCollection_IfRecordsStartsWithHash()
        {
            var records = new string[] { "# Material inventory initial state as of Jan 01 2018", "#Material inventory initial state as of Jan 01 2018" };
            var processor = new InputProcessor();

            var result = processor.ProcessRecordsToWarehouseMaterials(() => records);

            Assert.Empty(result);
        }

        [Fact]
        void RecordsProcessing_ShouldReturnEmptyCollection_IfNoRecords()
        {
            var records = new string[0];
            var processor = new InputProcessor();

            var result = processor.ProcessRecordsToWarehouseMaterials(() => records);

            Assert.Empty(result);
        }

        [Fact]
        void RecordsProcessing_ShoudReturnWarehouseMaterialsOnly()
        {
            var processor = new InputProcessor();

            var result = processor.ProcessRecordsToWarehouseMaterials(TestRecords);

            Assert.Equal(3, result.ToList().Count);
        }

        [Fact]
        void RecordsProcessing_ShouldReturnValidWarehouseMaterials()
        {

            var processor = new InputProcessor();

            var result = processor.ProcessRecordsToWarehouseMaterials(TestRecords);

            Assert.Equal(5, result.First().Amount);
            Assert.Equal("WH-A", result.First().WarehouseId);
        }
    }
}