namespace Innergy.Exam.Models
{
    public class WarehouseMaterial
    {
        public string WarehouseId { get; private set; }
        public string MaterialId { get; private set; }
        public int Amount { get; private set; }

        public WarehouseMaterial(string materialId, string warehouseId, int amount)
        {
            WarehouseId = warehouseId;
            MaterialId = materialId;
            Amount = amount;
        }
        public WarehouseMaterial(string materialId, string warehouseRawRecord)
        {
            var data = warehouseRawRecord.Split(",");
            WarehouseId = data[0];
            MaterialId = materialId;
            Amount = int.Parse(data[1]);
        }
    }
}