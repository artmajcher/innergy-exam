using System.Collections.Generic;

namespace Innergy.Exam.Models
{
    public class WarehouseDto
    {
        public string WarehouseId { get; private set; }
        public int Total { get; private set; }
        public IEnumerable<MaterialDto> Materials { get; private set; }
        public WarehouseDto(string id, int total, IEnumerable<MaterialDto> materials)
        {
            WarehouseId = id;
            Total = total;
            Materials = materials;
        }
    }
}