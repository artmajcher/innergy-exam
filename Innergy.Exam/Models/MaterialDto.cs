namespace Innergy.Exam.Models
{
    public class MaterialDto
    {
        public string MaterialId { get; private set; }
        public int Amount { get; private set; }

        public MaterialDto(string materialId, int amount)
        {
            MaterialId = materialId;
            Amount = amount;
        }
    }
}