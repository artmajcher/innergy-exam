using System.Collections.Generic;
using System.Linq;
using Innergy.Exam.Models;

namespace Innergy.Exam
{
    public class OutputProcessor
    {
        public string GetWarehousesSummary(IEnumerable<WarehouseMaterial> materials)
        {
            var outputDtos = GroupMaterialsInWarehouses(materials);

            return SummarizeForStandardOutput(outputDtos);
        }

        private IEnumerable<WarehouseDto> GroupMaterialsInWarehouses(IEnumerable<WarehouseMaterial> materials)
        {
            return materials
                .GroupBy(g => g.WarehouseId)
                .Select(w => new WarehouseDto(
                    w.Key,
                    w.Sum(m => m.Amount),
                    w.Select(m => new MaterialDto(m.MaterialId, m.Amount))
                    .OrderBy(m => m.MaterialId)
                ))
                .OrderByDescending(w => w.Total)
                .ThenByDescending(w => w.WarehouseId);
        }

        private string SummarizeForStandardOutput(IEnumerable<WarehouseDto> warehouses)
        {
            string formattedOutput = "";

            foreach (var r in warehouses)
            {
                formattedOutput += $"\n{r.WarehouseId} (total {r.Total})\n";
                foreach (var m in r.Materials)
                    formattedOutput += $"{m.MaterialId}: {m.Amount}\n";
            }

            return formattedOutput;
        }
    }
}