﻿using System;
using System.Collections.Generic;

namespace Innergy.Exam
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputProcessor = new InputProcessor();
            var outputProcessor = new OutputProcessor();

            var materials = inputProcessor.ProcessRecordsToWarehouseMaterials(ReadRecords);
            // var materials = inputProcessor.ProcessRecordsToWarehouseMaterials(() => TestRecords);

            var result = outputProcessor.GetWarehousesSummary(materials);

            Console.WriteLine("Warehouses inventory summary:");
            Console.Write(result);
            Console.ReadKey();
        }

        public static IEnumerable<string> ReadRecords()
        {
            Console.WriteLine("Start typing and press enter to add a new record...");
            string line;
            while ((line = Console.ReadLine()) != String.Empty)
                yield return line;
        }

        private static string[] TestRecords = {
            "# Material inventory initial state as of Jan 01 2018",
            "# New materials",
            "Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10",
            "Maple Dovetail Drawerbox;COM-124047;WH-A,15",
            "Generic Wire Pull;COM-123906c;WH-A,10|WH-B,6|WH-C,2",
            "Yankee Hardware 110 Deg. Hinge;COM-123908;WH-A,10|WH-B,11",
            "# Existing materials, restocked",
            "Hdw Accuride CB0115-CASSRC - Locking Handle Kit - Black;CB0115-CASSRC;WH-C,13|WH-B,5",
            "Veneer - Charter Industries - 3M Adhesive Backed - Cherry 10mm - Paper Back;3M-Cherry-10mm;WH-A,10|WH-B,1",
            "Veneer - Cherry Rotary 1 FSC;COM-123823;WH-C,10",
            "MDF, CARB2, 1 1/8';COM-101734;WH-C,8"
        };
    }
}