using System;
using System.Collections.Generic;
using System.Linq;
using Innergy.Exam.Models;

namespace Innergy.Exam
{
    public class InputProcessor
    {
        public bool IsRecordValid(string record) => !string.IsNullOrWhiteSpace(record) && record.First() != '#';

        public IEnumerable<WarehouseMaterial> ProcessRecordsToWarehouseMaterials(Func<IEnumerable<string>> recordsSource)
        {
            var materials = new List<WarehouseMaterial>();
            foreach (var r in recordsSource())
                if (IsRecordValid(r))
                    materials.AddRange(ProcessRecord(r));

            return materials;
        }

        private IEnumerable<WarehouseMaterial> ProcessRecord(string record)
        {
            var materialData = record.Split(';');
            string materialId = materialData[1];
            var warehouseRecords = materialData[2].Split("|");

            return warehouseRecords.Select(w => new WarehouseMaterial(materialId, w));
        }
    }
}